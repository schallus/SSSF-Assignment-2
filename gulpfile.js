var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    del = require('del'),
    concat = require('gulp-concat');

/*
    Task which compile main.scss and minify the code
*/
gulp.task('styles', function() {
  return sass('assets/scss/main.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('public/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/css'))
    .pipe(notify({ message: 'Styles task complete' }));
});

/*
    Task which compile bootstrap.scss and minify the code
*/
gulp.task('bootstrap', function() {
  return sass('assets/scss/bootstrap.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('public/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/css'))
    .pipe(notify({ message: 'Bootstrap task complete' }));
});

//script paths
var jsFiles = [
        'assets/scripts/main.js/',
    ],
    jsDest = 'public/js';

/*
    Task which concat all my scripts and minify the code
*/
gulp.task('scripts', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest));
});

/*
    Task which clean the CSS before to regenerate them
*/
gulp.task('clean', function() {
    return del(['public/css/*']);
});


/*
    Default task, ran by using $ gulp, to run all three tasks we have created
*/
gulp.task('default', ['clean'], function() {
    gulp.start('styles');
});

/* Watch */
gulp.task('watch', function() {
  /* Watch .scss files */
  gulp.watch('assets/scss/main.scss', ['styles']);
  gulp.watch('assets/scss/bootstrap/*.scss', ['bootstrap']);
  gulp.watch('assets/scripts/**/*.js', ['scripts']);
});
const express = require('express');
const mongoose = require('mongoose'); // MongoDB package
mongoose.Promise = global.Promise; //ES6 Promise
const multer = require('multer'); // Middleware for handling multipart/form-data
const sharp = require('sharp'); // Middleware for image optimisation
const bodyParser = require('body-parser'); // Allow you to parse body data (ex: request.body.user.name)

require('dotenv').config();

// Manage database connection
const DB = require('./model/database');

// Get model
const model = require('./model/model');

// Upload folder for multer
const upload = multer({
    dest: 'public/uploads/',
});

const app = express();

DB.connect().then(() => {
    console.log('Connected to MongoDB server');
    app.listen(process.env.APP_PORT);
    console.log(`Listening on port ${process.env.APP_PORT}`);
}, (err) => {
    console.log(err.message);
    console.error('Connection to MongoDB server failed');
});

// Public html folder
app.use(express.static('public'));

/* // Use the Middleware body-parser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); */

const apiRouter = express.Router();

app.use('/spyApi', apiRouter);

apiRouter.get('/surveillances', (req, res) => {
    console.log('router /surveillances');
    model.Surveillance.find().limit(10).sort({date: -1}).exec().then((surveillances) => {
        console.log(surveillances);
        res.json(surveillances);
    }).catch((err) => {
        console.log(err);
    });
});

const uploadImage = upload.single('image');

apiRouter.post('/addSurveillance', (req, res) => {
    console.log('router /addSurveillance');

    uploadImage(req, res, (err) => {
        if (err) {
            // An error occurred when uploading
            return console.log('An error occurred when uploading');
        }

        // Everything went fine
        const surveillance = req.body.surveillance;
        const image = req.file;

        const createThumbnail = new Promise((resolve, reject) => {
            const destination = `/uploads/320x300-${image.filename}`;
            sharp(image.path)
                .resize(320, 300)
                .jpeg({
                    'quality': 60,
                })
                .toFile('public' + destination)
                .then(() => {
                    resolve(destination);
                }).catch((err) => {
                    reject(err);
                });
        });

        const createImage = new Promise((resolve, reject) => {
            const destination = `/uploads/768x720-${image.filename}`;
            console.log(image);
            sharp(image.path)
                .resize(768, 720)
                .jpeg({
                    'quality': 60,
                })
                .toFile('public' + destination)
                .then(() => {
                    resolve(destination);
                }).catch((err) => {
                    reject(err);
                });
        });

        Promise.all([createThumbnail, createImage]).then((destinations) => {
            const thumbnailUrl = destinations[0];
            const imageUrl = destinations[1];
            const originalImageUrl = image.destination + image.filename;

            const newSurveillance = {
                id: 12,
                time: new Date(),
                category: surveillance.category,
                title: surveillance.title,
                details: surveillance.description,
                coordinates: JSON.parse(surveillance.location),
                thumbnail: thumbnailUrl,
                image: imageUrl,
                original: originalImageUrl,
            };
            console.log(newSurveillance);

            model.Surveillance.create(newSurveillance).then((surveillance) => {
                console.log(surveillance);
                /* res.json({
                    created: true,
                    surveillance: surveillance,
                }); */

                res.redirect('/');
            }).catch((err) => {
                res.status(404).json({
                    created: false,
                    error: 'Error creating the surveillance',
                });
            });
        }).catch((err) => {
            res.status(404).json({
                created: false,
                error: 'Error processing the image',
            });
        });
    });
});

app.all('*', (req, res) => {
    if(res.status(404)) {
        res.status(404).send('<h1>Error 404 - Page unavailable</h1>');
    }
});

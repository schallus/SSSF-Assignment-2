/* global $ jQuery google document Headers Request fetch */

'use strict';

let currentCategory;
let surveillanceObject;
let map;
let marker;

// Default map center
let mapCenter = new google.maps.LatLng({
    lat: 60.184200,
    lng: 24.934500,
});

const displayFilters = () => {
    const surveillanceCategory = $('#surveillanceCategory');
    
    surveillanceCategory.find('input').not('input[data-filter=All]').parent('label').remove();
    surveillanceCategory.find('input').not('input[data-filter=All]').remove();

    const categories = getSurveillancesCategories();
    for(const cat of categories) {
        const filterButton = $(`<label class="btn btn-secondary">
            <input type="radio" name="category" data-filter="${cat}" autocomplete="off">${cat}
        </label>`);

        filterButton.appendTo(surveillanceCategory);
    }

    surveillanceCategory.find('input[name="category"]').on('change', (event) => {
        event.preventDefault();

        // Get the clicked category
        const clickedCategory = $(event.currentTarget).data('filter');

        // If the category changed, we display the surveillance items
        if(currentCategory != clickedCategory) {
            currentCategory = clickedCategory;

            displaySurveillances(currentCategory);
        }
    });
};

const displaySurveillances = (category = 'All') => {

    const surveillanceList = $('#surveillanceList');

    // Delete all the surveillance items
    surveillanceList.html('');

    // Filter the element
    const surveillanceArray = getSurveillancesByCategory(category);

    if(surveillanceArray.length === 0) {
        surveillanceList.append(`<div class="alert alert-warning">
            <strong>Error!</strong> There is nothing to display in this category
        </div>`);
    } else {
        for(const surveillance of surveillanceArray) {
            const surveillanceItem = `<div class="col-lg-4 col-md-6 col-xs-12">
                <div class="card">
                    <img class="card-img-top" src="${surveillance.thumbnail}" alt="${surveillance.title}">
                    <div class="card-block">
                        <h4 class="title">${surveillance.title}</h4>
                        <div class="card-text">
                            <p class="date">${new Date(surveillance.time).toLocaleString()}</p>
                            <p class="details">${surveillance.details}</p>
                            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-id="${surveillance._id}">View</button></p>
                        </div>
                    </div>
                </div>`;

            // Add an item to the list
            surveillanceList.append(surveillanceItem);
        }

        // On view button click
        $(surveillanceList).find('button').on('click', (event) => {
            // We get the clicked surveillance object
            const surveillanceId = $(event.currentTarget).data('id');
            const surveillance = getSurveillanceById(surveillanceId);

            // Add a marker on the map to the surveillance coordinates
            mapAddMarker(
                surveillance.coordinates.lat,
                surveillance.coordinates.lng,
                surveillance.title
            );

            // Create a modal and display it
            const modal = $('#surveillanceDetails');
            modal.find('.modal-title').text(surveillance.title);

            const modalBody = modal.find('.modal-body');

            const surveillanceImage = $('<img>').attr({
                src: surveillance.image,
            });
            modalBody.find('#surveillanceImage').html(surveillanceImage);

            $('#surveillanceDetails').modal({
                'backdrop': true,
                'show': true,
                'keyboard': true,
            }).on('shown.bs.modal', () => {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(mapCenter);
            });
        });
    }
};

const getSurveillanceById = (id) => {
    return surveillanceObject.filter((surveillance) => surveillance._id == id)[0];
};

const getSurveillancesByCategory = (category) => {
    if(category == 'All') {
        return surveillanceObject;
    } else {
        return surveillanceObject.filter((surveillance) => surveillance.category == category);
    }
};

const getSurveillancesCategories = () => {
    const categories = surveillanceObject.map((surveillance) => surveillance.category)
        .filter((elem, pos, arr) => arr.indexOf(elem) === pos);
    return categories;
};

const initMap = () => {
    map = new google.maps.Map($('#surveillanceLocation').get(0), {
        center: mapCenter,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });
};

const mapAddMarker = (lat, lng, title) => {
    // Remove the marker if there is already one
    if(marker !== undefined) {
        marker.setMap(null);
    }

    mapCenter = new google.maps.LatLng(lat, lng);

    marker = new google.maps.Marker({
        position: mapCenter,
        map: map,
        title: title,
    });

    // When we create a marker, we set the map center to the marker location
    map.setCenter(mapCenter);
};

const fetchJSONData = (url, callback) => {
    const myHeaders = new Headers({
        'Content-Type': 'application/json',
    });

    const myInit = {
        method: 'GET',
        headers: myHeaders,
        cache: 'default',
    };

    const myRequest = new Request(url, myInit);
    fetch(myRequest).then((response) => {
        if(response.ok) {
            const contentType = response.headers.get('Content-Type');
            if(contentType.indexOf('application/json') > -1) {
                return response.json();
            } else {
                throw new Error('The Content-Type is wrong.');
            }
        } else {
            throw new Error('Error loading the JSON content.');
        }
    }).then((json) => {
        callback(json);
    }).catch((e) => {
        console.log('Problem :( ' + e.message);

        $('#surveillanceList').append(`<div class="alert alert-danger">
            <strong>Error!</strong> ${e.message}
        </div>`);
    });
};

jQuery(document).ready(($) => {
    // Init the Google Map
    initMap();

    // Get data from the JSON file
    fetchJSONData('/spyApi/surveillances', (json) => {
        surveillanceObject = json;

        console.log(surveillanceObject);

        // Once data loaded, we display them
        displayFilters();
        displaySurveillances();
    });

    // On Google Map hover, we change the size of the map
    $('#surveillanceLocation').hover((event) => {
        $(event.currentTarget).animate({
            width: $(event.currentTarget).parent().width(),
            height: $(event.currentTarget).parent().height(),
        }, 200, () => {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapCenter);
        });
    }, (event) => {
        $(event.currentTarget).animate({
            width: '100px',
            height: '100px',
        }, 200, () => {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapCenter);
        });
    });

    const addSurveillanceForm = $('#addSurveillanceForm');

    addSurveillanceForm.submit((event) => {
        event.preventDefault();
        console.log(addSurveillanceForm.find('#inputLocation').val());
        if(addSurveillanceForm.find('#inputLocation').val()!='') {
            const geocoder = new google.maps.Geocoder();
            const address = addSurveillanceForm.find('#inputLocation').val();
            geocoder.geocode( { 'address': address}, function(results, status) {
                console.log(status);
                if (status === 'OK') {
                    const coordinates = {
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng(),
                    };

                    addSurveillanceForm.find('#inputLocation').val(JSON.stringify(coordinates));
                    addSurveillanceForm.unbind('submit');
                    addSurveillanceForm.submit();
                    displayFilters();

                } else {
                    alert('The address is incorrect');
                }
            });
        }
    });
});
